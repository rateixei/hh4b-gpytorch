# hh4b-gpytorch container

This is a container for running gpytorch on hh4b related matters

to run this image with singularity run the command for CPU optimised container
```
singularity exec --nv docker:/gitlab-registry.cern.ch/rateixei/hh4b-gpytorch/hh4b-gpytorch:latest bash
```

If you want to extend the image you can simply fork this repository and add additional packages in the [`Dockerfile`](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-images/ml-gpu/blob/master/Dockerfile) and possibly create a merge request.
with the flag `-B` you can add other directories to the container of needed.
